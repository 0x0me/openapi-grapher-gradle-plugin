plugins {
    kotlin("jvm") version "1.4.10"
    id("java-gradle-plugin")
}

group = "dev.mindcrime.gradle.oag"
version = "0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

gradlePlugin {
    plugins {
        create("openapigrapher") {
            id = "dev.mindcrime.openapi-grapher"
            implementationClass = "dev.mindcrime.gradle.oag.OpenApiGrapherPlugin"
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.swagger.parser.v3:swagger-parser:2.0.22")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.4.2")
}


tasks.test { useJUnitPlatform() }