package dev.mindcrime.gradle.oag

import java.io.Writer

class PlantUmlWriter(val writer: Writer) {

}

class DatatypeWriter(private val writer: Writer, private val configuration: GraphGeneratorConfiguration) {
    fun write(datatype: Datatype) {
        with(writer) {
            appendln("class ${datatype.name} << (D, white) DataType >>{")
            val pw = PropertyWriter(this, configuration)
            datatype.properties.forEach {
                pw.write(it)
                appendln()
            }
            appendln("}")

            // write seperate information about enums -> generate specific enum type to refer to
            if (!configuration.enumAsConstraints) {
                datatype.properties.filterIsInstance<EnumProperty<Any>>().forEach(EnumWriter(this)::writeType)
            }

            // write seperate information about associations -> generate association
            datatype.properties.filterIsInstance<AssocicationProperty>().forEach(AssocicationWriter(this)::writeType)

        }
    }
}

/**
 * Writes out a anonymous enum data type enumerating it values
 */
class EnumWriter(private val writer: Writer) {
    fun writeType(enumType: EnumProperty<Any>) {
        with(writer) {
            appendln("enum ${enumType.generateTypeName()} <<Anonymous>>{")
            enumType.values.forEach { appendln("\t$it") }
            appendln("}")
            appendln()
            appendln("${enumType.owningType} *-- ${enumType.generateTypeName()}")
        }
    }
}

/**
 * Writes out an association for given property and the containing class
 */
class AssocicationWriter(private val writer: Writer) {
    fun writeType(association: AssocicationProperty) {
        with(writer) {
            appendln("${association.owningType} o-- ${association.dataType}")
        }
    }
}

/**
 * Writes out a class property (inside a class structure)
 */
class PropertyWriter(private val writer: Writer, private val configuration: GraphGeneratorConfiguration) {


    fun write(property: ClassProperty) {
        //TODO this code does not look like idiomatic Kotlin code - from an OO point of view this
        // should work simply using method overloading and type hierarchies, but the compiler complains
        // about 'None of the following functions can be called with the arguments supplied'
        when (property) {
            is CommonProperty -> write(property)
            is AssocicationProperty -> write(property)
            is EnumProperty<*> -> write(property)
            else -> throw IllegalArgumentException("Cannot write information for a property of type ${property.javaClass}.class")
        }
    }

    fun write(property: AssocicationProperty) {
        with(writer) {
            append("\t${property.name} : ${property.dataType}")
        }
    }

    fun write(property: CommonProperty) {
        with(writer) {
            if (property.hasFlavor()) {
                append("\t<<${property.dataType}>> ${property.name} : ${property.flavor}")
            } else {
                append("\t${property.name} : ${property.dataType}")
            }
        }
    }

    fun write(property: EnumProperty<*>) {
        with(writer) {
            if (configuration.enumAsConstraints) {
                append("\t${property.name} : ${property.dataType}")
                append(" = {").append(property.getPropertyConstraints()).append("}")
            } else {
                append("\t${property.name} : ${property.generateTypeName()}")
            }
        }
    }

}