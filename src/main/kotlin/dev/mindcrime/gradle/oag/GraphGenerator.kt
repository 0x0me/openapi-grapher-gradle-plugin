package dev.mindcrime.gradle.oag

import io.swagger.parser.util.RefUtils
import io.swagger.v3.parser.OpenAPIV3Parser
import io.swagger.v3.parser.core.models.ParseOptions
import java.io.File
import java.io.Writer
import java.util.*
import javax.xml.crypto.Data

data class GraphGeneratorConfiguration(val enumAsConstraints: Boolean = true)

data class Datatype(val name: String) {
    private val _properties = mutableListOf<ClassProperty>()
    val properties: List<ClassProperty>
        get() = _properties


    fun addProperty(property: ClassProperty) = _properties.add(property)
    fun clearProperties() = _properties.clear()
}

interface ClassProperty {
    val owningType: String
    val name: String
    val dataType: String
}

data class CommonProperty(
    override val owningType: String,
    override val name: String,
    override val dataType: String,
    val flavor: String? = null
) : ClassProperty {
    fun hasFlavor(): Boolean = flavor != null
}

data class EnumProperty<T>(
    override val owningType: String,
    override val name: String,
    override val dataType: String,
    val values: List<T>
) :
    ClassProperty {
    fun generateTypeName(): String = name.capitalize() + owningType.capitalize() + "Enum"
    fun getPropertyConstraints(): String = values.joinToString(",") { if (dataType == "string") "\"$it\"" else "$it" }
}

data class AssocicationProperty(
    override val owningType: String,
    override val name: String,
    override val dataType: String
) : ClassProperty {
}


class GraphGenerator(
    private val specificationFile: File,
    private val configuration: GraphGeneratorConfiguration = GraphGeneratorConfiguration()
) {
    fun generate(output: Writer) {
        val options = ParseOptions()

        val apiSpecification = OpenAPIV3Parser().read(specificationFile.absolutePath, null, options)
        val schemas = apiSpecification.components.schemas


        output.appendln("@startuml")
        schemas.keys.forEach { classContainer ->
            val dataType = Datatype(classContainer)

            schemas[classContainer]?.properties?.forEach { p ->

                val schemaForCurrentType = p.value
                val flavor = schemaForCurrentType.format
                val propertyName = p.key
                val isEnum = !schemaForCurrentType.enum.isNullOrEmpty()

                val type = Optional.ofNullable(schemaForCurrentType.type).orElseGet {
                    RefUtils.computeDefinitionName(schemaForCurrentType.`$ref`)
                }

                val property: ClassProperty =
                    when {
                        schemaForCurrentType.type == null -> {
                            AssocicationProperty(classContainer, propertyName, type) as ClassProperty
                        }
                        isEnum -> {
                            val enumValues = schemaForCurrentType.enum.map { it }
                            EnumProperty(classContainer, propertyName, type, enumValues) as ClassProperty
                        }
                        else -> {
                            CommonProperty(classContainer, propertyName, type, flavor) as ClassProperty
                        }
                    }

                dataType.addProperty(property)
            }

            //
            DatatypeWriter(output, configuration).write(dataType)
            dataType.clearProperties()
        }
        output.appendln("@enduml")
    }

}