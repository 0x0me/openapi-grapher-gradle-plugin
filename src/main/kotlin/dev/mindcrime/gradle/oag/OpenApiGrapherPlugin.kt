package dev.mindcrime.gradle.oag

import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.tasks.*
import java.io.File
import java.io.FileWriter


private const val EXTENSTION_NAME = "openapigraph"

open class OpenApiGrapherPlugin : Plugin<Project> {
    override fun apply(project: Project) {

        // create extension
        val openapigraph = project.extensions.run { create(EXTENSTION_NAME, OpenApiGrapherExtension::class.java, project) }

        // make task in project available
        with(project.tasks) {
            create("generateGraph", OpenApiGrapherTask::class.java) {
                it.group = "documentation"
                it.description = "Writes OpenAPI schema elements to a PlantUML diagram in an asciidoctor file."

                it.specification.set(openapigraph.spec)
            }
        }
    }
}

/**
 * Extension definition for 'openapigraph' settings block.
 *
 * <code>
 *     openapigraph {
 *          spec = file("path/to/openapi/specification.yaml")
 *     }
 * </code>
 */
open class OpenApiGrapherExtension(project: Project) {
    val spec = project.objects.fileProperty()
}

// per default the output will be written to specified location
private const val OUTPUT_FILE = "openapigrapher/schemagraph.asciidoc"

/**
 * The actual graph generation task converting the schema definitions
 * into a PlantUML representation
 */
open class OpenApiGrapherTask : DefaultTask() {
    @OutputFile
    var output: File = project.file("${project.buildDir}/$OUTPUT_FILE")

    @get:InputFile
    @PathSensitive(PathSensitivity.RELATIVE)
    val specification: RegularFileProperty = project.objects.fileProperty()

    @TaskAction
    fun generateGraph() {
        logger.quiet("Generating Graph for")

        if (!specification.isPresent) {
            throw InvalidUserDataException("The property `spec` has to refer to a valid and readable OpenAPI specification")
        }

        println("Generating graph for specification $specification")

        val generator = GraphGenerator(specificationFile = specification.asFile.get())
        FileWriter(output).use {
            generator.generate(it)
        }

        println("succesfull generated ${output.absoluteFile}")
    }
}
