package dev.mindcrime.gradle.oag

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class OpenApiGrapherPluginTest {
    @Test
    fun `project should have a registered "generateGraph" task`() {
        val project: Project = ProjectBuilder.builder().build()
        project.pluginManager.apply("dev.mindcrime.openapi-grapher")
        assertTrue(project.tasks.getByName("generateGraph") is OpenApiGrapherTask)
    }
}