package dev.mindcrime.gradle.oag

import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Path

internal class OpenApiGrapherTaskTest {
    @TempDir
    lateinit var testProjectDir: Path
    private lateinit var settingsFile: File
    private lateinit var buildFile: File

    @BeforeEach
    fun setup() {
        settingsFile = testProjectDir.resolve("settings.gradle").toFile()
        buildFile = testProjectDir.resolve("build.gradle").toFile()
    }

    @Test
    fun `test generateGraph task`() {

        val testSpecificationFile = File("src/test/resources/minimal.yaml").absoluteFile

        settingsFile.writeText(
            """
            rootProject.name = "hello-world"
        """.trimIndent()
        )
        buildFile.writeText(
            """
            plugins {
                id ("dev.mindcrime.openapi-grapher")
            }
            
            openapigraph {
                spec = file("$testSpecificationFile")
            }
        """.trimIndent()
        )

        val result = GradleRunner.create()
            .withTestKitDir(Files.createTempDirectory(testProjectDir, "").toFile())
            .withProjectDir(testProjectDir.toFile())
            .withArguments("generateGraph", "--stacktrace")
            .withDebug(true)
            .forwardStdOutput(PrintWriter(System.out))
            .withPluginClasspath()
            .build()

        assertEquals(TaskOutcome.SUCCESS, result.task(":generateGraph")?.outcome)

        val output = File(testProjectDir.toFile(), "build/openapigrapher/schemagraph.asciidoc")
        assert(output.exists())
        Files.readAllLines(output.toPath()).map(::println)
    }

    @Test
    fun `should fail if there is no valid specification file provided`() {

        settingsFile.writeText(
            """
            rootProject.name = "hello-world"
        """.trimIndent()
        )
        buildFile.writeText(
            """
            plugins {
                id ("dev.mindcrime.openapi-grapher")
            }
        """.trimIndent()
        )

        GradleRunner.create()
            .withTestKitDir(Files.createTempDirectory(testProjectDir, "").toFile())
            .withProjectDir(testProjectDir.toFile())
            .withArguments("generateGraph", "--stacktrace")
            .withDebug(true)
            .forwardStdOutput(PrintWriter(System.out))
            .withPluginClasspath()
            .buildAndFail()
    }
}