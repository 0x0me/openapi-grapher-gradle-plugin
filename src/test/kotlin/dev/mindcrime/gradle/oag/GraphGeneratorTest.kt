package dev.mindcrime.gradle.oag

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.File
import java.io.StringWriter

internal class GraphGeneratorTest {
    @Test
    fun `should convert a minimal api specification`() {
        val expected = """
@startuml
class Sample << (D, white) DataType >>{
	<<integer>> id : int64
	name : string
	<<integer>> quantity : int32
	<<string>> timestamp : date-time
	status : string = {"new","open","finished"}
	complete : boolean
}
@enduml
""".trimIndent()
        val sw = StringWriter()
        GraphGenerator(File("src/test/resources/minimal.yaml")).generate(sw)
        assertEquals(expected, sw.toString().trimIndent())
    }

    @Test
    fun `should generate a constraint value definition for enum properties`() {
        val expected = """
@startuml
class Sample << (D, white) DataType >>{
	status : string = {"new","open","finished"}
}
@enduml
""".trimIndent()

        val sw = StringWriter()
        GraphGenerator(File("src/test/resources/minimal-enum.yaml")).generate(sw)

        assertEquals(expected, sw.toString().trimIndent())
    }

    @Test
    fun `should generate a separate enum class for enum properties`() {
        val expected = """
@startuml
class Sample << (D, white) DataType >>{
	status : StatusSampleEnum
}
enum StatusSampleEnum <<Anonymous>>{
	new
	open
	finished
}

Sample *-- StatusSampleEnum
@enduml
""".trimIndent()

        val sw = StringWriter()
        GraphGenerator(File("src/test/resources/minimal-enum.yaml"), GraphGeneratorConfiguration(false)).generate(sw)

        assertEquals(expected, sw.toString().trimIndent())
    }

    @Test
    fun `should generate a separate enum class for enum properties on same properties in different objects`() {
        val expected = """
@startuml
class Sample << (D, white) DataType >>{
	status : StatusSampleEnum
}
enum StatusSampleEnum <<Anonymous>>{
	new
	open
	finished
}

Sample *-- StatusSampleEnum
class Data << (D, white) DataType >>{
	status : StatusDataEnum
}
enum StatusDataEnum <<Anonymous>>{
	1
	2
	3
}

Data *-- StatusDataEnum
@enduml
""".trimIndent()

        val sw = StringWriter()
        GraphGenerator(File("src/test/resources/minimal-enum2.yaml"), GraphGeneratorConfiguration(false)).generate(sw)

        assertEquals(expected, sw.toString().trimIndent())
    }


    @Test
    fun `should handle complex attributes`() {
        val expected = """
@startuml
class Sample << (D, white) DataType >>{
	<<integer>> id : int64
	assoc : Association
}
Sample o-- Association
class Association << (D, white) DataType >>{
	<<integer>> id : int64
	qualifier : string
}
@enduml
""".trimIndent()

        val sw = StringWriter()
        GraphGenerator(File("src/test/resources/minimal-assoc.yaml"), GraphGeneratorConfiguration(false)).generate(sw)

        assertEquals(expected, sw.toString().trimIndent())
    }


}